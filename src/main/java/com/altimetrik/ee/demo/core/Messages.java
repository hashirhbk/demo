package com.altimetrik.ee.demo.core;

public class Messages {

    public static final String ERROR_ID_DOESNT_EXIST = "The id doesn't exist";

    private Messages() {
    }
}
