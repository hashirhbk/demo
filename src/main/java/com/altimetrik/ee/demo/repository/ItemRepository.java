package com.altimetrik.ee.demo.repository;


import com.altimetrik.ee.demo.model.entity.Item;
import com.altimetrik.ee.demo.model.entity.ItemPk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, ItemPk> {

}
